<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Category extends Model
{
    use HasFactory, SoftDeletes;

    protected $dates = ['deleted_at'];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'description',
    ];

    public function books () {
    	return $this->hasMany(Book::class);
    }

    // Accesor para obtener la fecha en el formato con decimales para SQLServer
    public function getDateFormat() {
        return 'Y-m-d H:i:s.u';
    }
}
