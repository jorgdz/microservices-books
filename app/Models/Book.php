<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Book extends Model
{
    use HasFactory, SoftDeletes;

    protected $dates = ['deleted_at'];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'title', 'description', 'price', 'author_id', 'category_id'
    ];

    public function category () {
        return $this->belongsTo(Category::class);
    }

    // Accesor para obtener la fecha en el formato con decimales para SQLServer
    public function getDateFormat() {
        return 'Y-m-d H:i:s.u';
    }
}
