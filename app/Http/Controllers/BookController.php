<?php

namespace App\Http\Controllers;

use App\Models\Book;
use App\Traits\RestResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Exceptions\Custom\UnprocessableException;

class BookController extends Controller
{

    use RestResponse;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Return book list
     * @return Illuminate\Http\Response
     */
    public function index () {
        $books = Book::with('category')
            ->orderBy('id', 'desc')
            ->paginate(10);

        return $this->success($books);
    }

    /**
     * Create an instance of book
     * @return Illuminate\Http\Response
     */
    public function store (Request $request) {
        $rules = [
            'title' => 'required|max:255|unique:books',
            'description' => 'required|max:255',
            'price' => 'required|min:1',
            'author_id' => 'required|min:1',
            'category_id' => 'required',
        ];

        $this->validate($request, $rules);

        $book = Book::create($request->all());

        return $this->success($book, Response::HTTP_CREATED);
    }

    /**
     * Return a one book
     * @return Illuminate\Http\Response
     */
    public function show ($book) {
        $book = Book::with('category')->findOrFail($book);
        return $this->success($book);
    }

    /**
     * Update the information of an existing book
     * @return Illuminate\Http\Response
     */
    public function update (Request $request, $book) {
        $rules = [
            'title' => 'max:255|unique:books,title,'.$book,
            'description' => 'max:255',
            'price' => 'min:1',
            'author_id' => 'min:1',
        ];

        $this->validate($request, $rules);

        $book = Book::findOrFail($book);

        $book->fill($request->all());

        if ($book->isClean())
            throw new UnprocessableException(__('messages.nochange'));

        $book->save();

        return $this->success($book);
    }

    /**
     * Removes an existing book
     * @return Illuminate\Http\Response
     */
    public function destroy ($book) {
        $book = Book::findOrFail($book);

        $book->delete();

        return $this->success($book);
    }
}
